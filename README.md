水果分拣机数字孪生系统
======
## 介绍
> 数字孪生是充分利用物理模型、传感器更新、运行历史等数据，集成多学科、多物理量、多尺度、多概率的仿真过程，在虚拟空间中完成映射，从而反映相对应的实体装备的全生命周期过程。简单来讲，就是实现现实物理系统向虚拟空间数字化模型的反馈。

> 本项目是针对特定水果分拣机器而制作的数字孪生系统，团队成员来自广轻软件195班，项目性质为练手，所以实际应用意义不大（

## 现有功能
* 实时显示机器状态
* 下达指令控制机器
* 接收特定报告信息
## 将来计划
* 模拟展示传送带上物体
* 智能控制推杆
* 完善报告接收接口，以接收所有机器报告消息
## 页面预览
![预览](imgs/preview.png "页面预览")
## 主要架构、使用类库
* 前端 `three.js`
* 数据传输 `axios` `socket`
* 后端 `spring boot`