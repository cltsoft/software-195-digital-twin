//该js用于初始化前端页面

const modelPath = '../static/models/machine.glb'

import * as utils from './utils.js'

utils.init(modelPath)

//记录日志
let info = document.querySelector('#info')
function log(text, level){
    info.innerHTML = '<p class="info-' + level + '">' + text + '</p>' + info.innerHTML
}

//加载当前机器信息
log('查询机器状态...', 'primary')
axios.get('/current').then(res => {
    if (res.data.status === 0){
        for (let i = 0; i < 6; i++){
            if (res.data.pusher[i] === true){
                log((i + 1) + '号推杆正常', 'success')
            } else {
                log((i + 1) + '号推杆故障', 'danger')
            }
        }
    } else {
        log('查询失败，未能连接机器', 'danger')
    }
})

//菜单初始化
let titles = document.querySelectorAll('.title')
for (let i = 0; i < titles.length; i++) {
    titles[i].addEventListener('click', e => {
        e.target.parentNode.classList.toggle('closed')
    })
}

let ranges = document.querySelectorAll('input[type=range]')
for (let i = 0; i < ranges.length; i++) {
    ranges[i].addEventListener('mousemove', e => {
        document.querySelector('label[for=' + e.target.id + ']').innerHTML = e.target.value
    })
}

//触发推杆动画
let hold_range = document.querySelector('#hold-range')
let pushers = document.querySelectorAll('button.push')
for (let i = 0; i < pushers.length; i++) {
    pushers[i].addEventListener('click', e => {
        e.target.setAttribute('disabled', '')
        axios.get('/push',{
            params: {
                num: e.target.getAttribute('data-id'),
                hold: hold_range.value * 10
            }
        }).then(evt => {
            if (evt.data.status === 0){
                utils.push(e.target.getAttribute('data-id'), parseFloat(hold_range.value))
                log(e.target.getAttribute('data-id') + '号推杆触发成功', 'success')
            } else {
                log('触发失败，未能连接机器', 'danger')
            }
            e.target.removeAttribute('disabled')
        })
    })
}

//触发传送带动画
let short_speed = document.querySelector('#belt-s-range')
let long_speed = document.querySelector('#belt-l-range')

let short_rolling = false
let long_rolling = false

document.querySelector('#belt-s').onclick = e => {
    e.target.setAttribute('disabled', '')
    if (short_rolling) {
        axios.get('/stopBelt',{
            params: {
                num: 1
            }
        }).then(evt => {
            if (evt.data.status ===  0){
                utils.stopShort()
                short_rolling = false
                e.target.innerHTML = '启动'
                short_speed.removeAttribute('disabled')
                e.target.classList.toggle('danger')
                log('上货端传送带停止成功', 'success')
            } else {
                log('触发失败，未能连接机器', 'danger')
            }
            e.target.removeAttribute('disabled')
        })
    } else {
        axios.get('/startBelt',{
            params: {
                num: 1,
                speed: parseInt(short_speed.value)
            }
        }).then(evt => {
            if (evt.data.status === 0){
                utils.rollShort(parseInt(short_speed.value))
                short_rolling = true
                e.target.innerHTML = '停止'
                short_speed.setAttribute('disabled', '')
                e.target.classList.toggle('danger')
                log('上货端传送带启动成功', 'success')
            } else {
                log('触发失败，未能连接机器', 'danger')
            }
            e.target.removeAttribute('disabled')
        })
    }
}
document.querySelector('#belt-l').onclick = e => {
    e.target.setAttribute('disabled', '')
    if (long_rolling) {
        axios.get('/stopBelt',{
            params: {
                num: 2,
            }
        }).then(evt => {
            if (evt.data.status === 0){
                utils.stopLong()
                long_rolling = false
                e.target.innerHTML = '启动'
                long_speed.removeAttribute('disabled')
                e.target.classList.toggle('danger')
                log('分拣端传送带停止成功', 'success')
            } else {
                log('触发失败，未能连接机器', 'danger')
            }
            e.target.removeAttribute('disabled')
        })
    } else {
        axios.get('/startBelt',{
            params: {
                num: 2,
                speed: parseInt(long_speed.value)
            }
        }).then(evt => {
            if (evt.data.status === 0){
                utils.rollLong(parseInt(long_speed.value))
                long_rolling = true
                e.target.innerHTML = '停止'
                long_speed.setAttribute('disabled', '')
                e.target.classList.toggle('danger')
                log('分拣端传送带启动成功', 'success')
            } else {
                log('触发失败，未能连接机器', 'danger')
            }
            e.target.removeAttribute('disabled')
        })
    }
}