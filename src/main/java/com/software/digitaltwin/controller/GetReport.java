package com.software.digitaltwin.controller;

import com.software.digitaltwin.tcp.TCPUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;

@Controller
public class GetReport {

    @GetMapping("/getReport")
    @ResponseBody
    public String getReport(){
        byte[] report = TCPUtils.getMachineReport();
        return Arrays.toString(report);
    }
}
