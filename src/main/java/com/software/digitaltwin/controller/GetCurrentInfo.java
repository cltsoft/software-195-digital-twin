package com.software.digitaltwin.controller;

import com.software.digitaltwin.tcp.TCPUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;

@Controller
public class GetCurrentInfo {

    @GetMapping("/current")
    @ResponseBody
    public String startBelt(){
        byte[] code = TCPUtils.getCurrentInfo();
        if (code == null) return "{\"status\": 1}";

//            String status_1 = Integer.toBinaryString(Byte.toUnsignedInt(code[2])); //状态1
//            String status_2 = Integer.toBinaryString(Byte.toUnsignedInt(code[3])); //状态2
        int status3 = Byte.toUnsignedInt(code[4]); //状态3

        boolean[] pusher_status = new boolean[6];
        for (int i = 0; i < 6; i++) {
            pusher_status[i] = (status3 & 1) == 1;
            status3 >>>= 1;
        }

        return "{\"status\": 0, \"pusher\": " + Arrays.toString(pusher_status) + "}";
    }
}
