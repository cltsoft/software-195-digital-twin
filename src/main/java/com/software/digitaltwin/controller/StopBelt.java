package com.software.digitaltwin.controller;

import com.software.digitaltwin.tcp.TCPUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class StopBelt {

    @GetMapping("/stopBelt")
    @ResponseBody
    public String startBelt(byte num){
        boolean success = TCPUtils.stopBelt(num);
        return success ? "{\"status\": 0}" : "{\"status\": 1}";
    }
}
