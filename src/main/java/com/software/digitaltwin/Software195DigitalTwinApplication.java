package com.software.digitaltwin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})

public class Software195DigitalTwinApplication {

    public static void main(String[] args) {
        SpringApplication.run(Software195DigitalTwinApplication.class, args);
    }

}
